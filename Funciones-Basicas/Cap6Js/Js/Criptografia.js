/**we say that only put value from 8bits  */
let ArrayBase = new Uint8Array(5);

/**we can put this values in any other array like value, example */

console.log(ArrayBase);

let ArrayComplete = [crypto.getRandomValues(ArrayBase), 'Alex', 'Lopez']

console.log(ArrayComplete);

/**we can recorrer only when we are object like params  */

ArrayComplete[0].forEach(e=> console.log(e));

/**New array with values from simple array */
/**we can just still add values to the Uint8Array with crypto.getRandomValues */

/**now we're gonna talk about ArrayBuffert */

let Buffer = new ArrayBuffer(9);
console.log(`this is our buffer ${Buffer.byteLength}`);


/**Cryptografia vanilla javascript  */

let Input = new TextEncoder('utf-8').encode('Hola Mundo.........');
console.log('encryptado',Input);

console.log(crypto.subtle.digest('SHA-256', Input).then(e=> console.log(e)));