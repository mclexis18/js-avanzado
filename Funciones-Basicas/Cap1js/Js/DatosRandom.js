/**Funciones dentro de arreglos */
const DatosRandom =
{
    Nombre:'Alexis',
    Datos(){
        console.log(this.Nombre)
    }
}

DatosRandom.Datos();

/**Funciones instanciadas */

function ShowName(name){

    this.name = name;

}

let Nombre = new ShowName('Linux');
console.log(Nombre);

/**Objeto this */


let VariableGlobal = this;

let Comida = (()=> this)
console.log(Comida() === VariableGlobal);

let ObjetoNuevo = {name:'comidafrita'};


/**Call es un metodo que sustituye a un objeto */
console.log(Comida.call(ObjetoNuevo)=== VariableGlobal);

/**Sin Importar en que lugar pongamos el this, siempre va a ser igual a otro this  */

let Objetwithwork = {

    withoutarrow(){
        return this;
    }

}

console.log('this is another side')

console.log(Objetwithwork.withoutarrow() === Objetwithwork);

/**Valores personalizados */

function Data1 (){

    console.log(this.Nombrecitos);

}

Data1.apply({Nombrecitos: 'Mili'});
Data1.call({Nombrecitos: 'Chiki'});


function Speaking(){

    let Frases = Array.prototype.slice.call(arguments);
    console.log(`${this.name} ${Frases}`);
}

let Persona = {name:'Linux'};

/**Apply permite agregar valores a una funcion en forma de matriz*/
Speaking.apply(Persona, ['love' ,'Hem'])

/**Call permite pasar valores a un documento unidos, no es tan sensible */

Speaking.call(Persona, 'Love hem');