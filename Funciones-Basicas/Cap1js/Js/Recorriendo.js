
/**En este contenido se recorreran arreglos y Objetos */
window.onload =()=>{

    const Contenido = document.getElementById('Contenido');

    const DatosPersonales = 
    {
    
        Nombre:'Alexis',
        Apellido:'Lopez'
    
    }
        
    const Recorrer = document.getElementById('Recorrer');
    Recorrer.addEventListener('click', ()=>{

        /**Convertimos el objeto en arreglo y lo recorremos con un for*/
        let ObjetoTranform = Object.keys(DatosPersonales);

        for (let index = 0; index < ObjetoTranform.length; index++) {

            /**Se transforma en valores y luego se pasa en el objeto */
            const Valores = ObjetoTranform[index];
            Contenido.innerHTML+= `<p>${DatosPersonales[Valores]}</p>`

        }

    });

    const Recorrer2 = document.getElementById('Recorrer2');
    Recorrer2.addEventListener('click', ()=>{

        let ObjetoConvertido = Object.values(DatosPersonales);
        
        for (let index = 0; index < ObjetoConvertido.length; index++) {
        
            Contenido.innerHTML+= `<p> ${ObjetoConvertido[index]}</p>` 
            
        }

    });

    const Recorrer3 = document.getElementById('Recorrer3');
    Recorrer3.addEventListener('click', ()=>{

        for (const Llave in DatosPersonales) {
           
            Contenido.innerHTML+= `<p>${DatosPersonales[Llave]}</p>`;

        }

    });

    const Recorrer4 = document.getElementById('Recorrer4');
    Recorrer4.addEventListener('click', ()=>{

        const Valores = Object.entries(DatosPersonales);
        Valores.forEach(([key ,value])=>{
            
            Contenido.innerHTML+= `<p>${value}</p>`

        })

    });

    const Recorrer5 = document.getElementById('Recorrer5');
    Recorrer5.addEventListener('click', ()=>{

        for (const [Key, Valor] of Object.entries(DatosPersonales)) {

            Contenido.innerHTML+= `<p>${Valor}</p>`
            
        }

    })

}

