window.addEventListener('load', ()=>{

    const Documentacion = document.getElementById('Documentation');
    Documentacion.innerHTML+= `<p>its me a new localstorage</p>`

    /**Create a new item with key and value */
    localStorage.setItem('name', 'Alexis Lopez');
    localStorage.setItem('lastname', 'López Mero');

    /**we get our dates storage in localstorage with getItem */
    console.log(localStorage.getItem('lastname'));

    /**if we wanted storage a object type json we must write like this   */

    const DatosEstudiante = [
        {
            Nombre:'Alexis',
            Edad:'22'
        },
        {
            Nombre:'Nardine',
            Edad:'23'
        }
    ]


    /**we become it in file type json */
    /**save it */
    localStorage.setItem('DatosEstudiantes', JSON.stringify(DatosEstudiante));
    /**if we Want to show that data well we'll do the next */

    /**delete it */
    localStorage.removeItem('lastname');

    /**Show it */
    console.log(JSON.parse(localStorage.getItem('DatosEstudiantes')));

    /**Delete all storage */
    //localStorage.clear();
    /**Note localStorage.clear delete everithyng value, but
     * don't delete her keys
     */

    /**there are another way to create o storage our data is the next way */

    localStorage.Colegio = 'Bahia de Manta'
    localStorage.Universidad= 'Uleam'

    console.log(localStorage.getItem('Colegio'));
    delete localStorage.Universidad

    console.log(localStorage.length)


});
