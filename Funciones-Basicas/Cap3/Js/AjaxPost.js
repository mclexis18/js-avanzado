/**Metodo post con XMLHTTPREQUEST */
window.addEventListener('load', ()=>{

    /**Obtenemos los datos de el formulario de FecthApiPost.html */
    const Titulo = document.getElementById('title').value;
    const Cuerpo = document.getElementById('body').value;
    const UserId = document.getElementById('userId').value;

    /**Creamos un objeto con esos datos */
    const Data = [{Titulo:Titulo, Cuerpo:Cuerpo, UserId:UserId}]

    /**Pasamos el Url en el cual los colocamos*/
    let Url = 'https://jsonplaceholder.typicode.com/posts';
    /**Hacemos la instancia de XMLHTTPREQUEST */
    let AjaxPost = new XMLHttpRequest();
    /**Llamamos el boton */
    const Btn1 = document.getElementById('Generar');
    
    /**Le agregamos un evento al boton  */
    Btn1.addEventListener('click', ()=>{

            /**Abrimos metodo, pasamos cabezera, y cuando este listo enviamos la respuesta  */        
            AjaxPost.open('POST', Url, true);
            AjaxPost.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
            /**whitout this methos don't work */
            AjaxPost.onreadystatechange =()=>{

                console.log(AjaxPost.responseText);

            }
            /**Enviamos el dato el cual hicimos con los valores del formulario */
            AjaxPost.send(JSON.stringify(Data));
            console.log(AjaxPost.responseText);

    });
})