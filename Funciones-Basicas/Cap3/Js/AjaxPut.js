window.addEventListener('load', ()=>{

    
    let AjaxPut = new XMLHttpRequest();

    const Id = document.getElementById('Id-Data').value;
    const Titulo = document.getElementById('title').value;
    const Cuerpo = document.getElementById('body').value;
    const UserId = document.getElementById('userId').value;
    const Btn = document.getElementById('GenerarPut');

    const Data = [{id:Id, title:Titulo, body:Cuerpo, userId:UserId}]

    Btn.addEventListener('click', ()=>{

        let Url = `https://jsonplaceholder.typicode.com/posts/${Id}`;
        
        AjaxPut.open('PUT', Url);
        AjaxPut.setRequestHeader('content-type', 'application/x-www-form-urlencoded')
        AjaxPut.onreadystatechange=()=>{

            console.log(AjaxPut.responseText);

        }

        AjaxPut.send(JSON.stringify(Data));
        console.log(AjaxPut.responseText);

    });


});