/**Como Agregar campos a un objeto con emas6 */

const ListaDeCompras = {}
const Verduras = {Silantro:false}
const Frutas = {Manzana:true}

/**El primer parametro para assing sera donde queremos agregar 
 * el segundo parametro, lo que agregamos, o podemos agregar mas de un parametro 
*/
Object.assign(ListaDeCompras, Frutas, Verduras, {Presa:'Carne'});


console.log(ListaDeCompras);